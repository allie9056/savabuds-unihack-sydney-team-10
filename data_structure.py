class User:

    def __init__(self, name='',password='' ,id=0, email='', age=0, savrpoints=0, spending_total=0, spending={'Food/Drinks':0,'Transport':0,'Entertainment':0,'Personal Care':0,'Other':0}, tree=0, saving=0.0, goals=[{'goal_description': "",'goal_amount': "",'date_to_complete_by': ""}], rank=0, achievements=[],friends=[]):
        self.name = name
        self.password = password
        self.id = id
        self.email = email
        self.age = age
        self.savrpoints = savrpoints
        self.spending_total = spending_total
        self.spending = spending
        self.tree = tree
        self.saving = saving
        self.goals = goals
        self.rank = rank
        self.achievements = achievements
        self.friends = friends

    def get_name(self):
        return self.name

    def set_name(self, value):
        self.name = value

    def re_initiate(self, dict):
        if 'name' in dict.keys():
            self.name = dict['name']
        if 'password' in dict.keys():
            self.password = dict['password']
        if 'id' in dict.keys():
            self.id = dict['id']
        if 'email' in dict.keys():
            self.email = dict['email']
        if 'age' in dict.keys():
            self.age = dict['age']
        if 'savrpoints' in dict.keys():
            self.savrpoints = dict['savrpoints']
        if 'spending' in dict.keys():
            self.spending = dict['spending']
        if 'spending_total' in dict.keys():
            self.spending_total = dict['spending_total']
        if 'tree' in dict.keys():
            self.tree = dict['tree']
        if 'saving' in dict.keys():
            self.saving = dict['saving']
        if 'goals' in dict.keys():
            self.goals = dict['goals']
        if 'rank' in dict.keys():
            self.rank = dict['rank']
        if 'achievements' in dict.keys():
            self.achievements = dict['achievements']
        if 'friends' in dict.keys():
            self.friends = dict['friends']



class Savrgroups:
    def __init__(self, name,users_involved, id):
        self.name = name
        self.users_involved = users_involved
        self.id = id

class quiz:
    def __init__(self, quiz_questions, quiz_answers):
        self.quiz_questions = quiz_questions
        self.quiz_answers = quiz_answers

class leaderboard:
    def __init__(self, the_leaderboard, rank):
        self.the_leaderboard = the_leaderboard
        self.rank = rank

class spending:
    def __init__(self, spending_total, spending_category):
        self.spending_total = spending_total
        self.spending_category = spending_category










