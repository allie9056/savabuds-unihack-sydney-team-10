from kivy.app import App
from kivy.lang import Builder

from kivy.properties import StringProperty
from kivy.uix.tabbedpanel import TabbedPanel

from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.camera import Camera

from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.screenmanager import SlideTransition

from kivy.core.window import Window
#from kivy.garden.widgetname import WidgetName


### Local Modules
from networking import Network
import data_structure


### Initial GUI setup
Builder.load_file('savabuds.kv')
Window.clearcolor = (.9,.9,.9,1) # Color background white


### Global account variables

local_user = data_structure.User()



class SuvrBudScreenManager(ScreenManager):
    def refresh_user_details(self,local_usr):
        for screen in self.screens:
            screen.refresh(local_usr)

# Initialise managers
main_screen_manager = SuvrBudScreenManager(transition=SlideTransition())
network_manage = Network(main_screen_manager, local_user)


### Custom Widgets

class Leaderboard(GridLayout):
    def __init__(self,l_user,**kwargs):
        super(Leaderboard,**kwargs).__init__(**kwargs)
        self.cols = 3



### Global account variables
spending_total= 0
local_user = data_structure.User()


class SavrScreen(Screen):
    def __init__(self,**kwargs):
        super(SavrScreen,self).__init__(**kwargs)
        self.local_usr = local_user
    def refresh(self,local_usr):
        self.local_usr = local_usr
    def on_pre_enter(self, *args):
        self.refresh(self.local_usr)

### Screens
class WelcomeScreen(SavrScreen):
    pass

class LoginScreen(SavrScreen):
    def login(self, usr_name,password):
        network_manage.connect_user(usr_name,password)

class SignUpScreen(SavrScreen):
    def signup(self, usr_name,password,email,age):
        network_manage.create_user(usr_name,password,email,age)

class HomeScreen(SavrScreen):
    pass

class BudsScreen(SavrScreen):

    def savrpoints_key(self,element):
        return element['savrpoints']

    def on_pre_enter(self, *args):
        self.to_delete = []

        friends = self.local_usr.friends
        if len(friends) > 0:
            friends = sorted(friends,key=self.savrpoints_key,reverse=True)
        for friend in friends:
            bud = Label(text="{}   {}".format(friend['name'],friend['savrpoints']),color=(0,0,0,1), size_hint=(1,0.1))
            self.ids.buds.add_widget(bud)
            self.to_delete.append(bud)
        space = Label(text='')
        self.ids.buds.add_widget(space)
        self.to_delete.append(space)

        QR_button = Button(text="Add Friends", size_hint=(1,0.1))
        self.ids.buds.add_widget(QR_button)
        self.to_delete.append(QR_button)


    def on_leave(self, *args):
        for i in self.to_delete:
            self.remove_widget(i)




class SavingsScreen(SavrScreen):

    def confirm_saving(self, saving):
        self.local_usr.saving += float(saving)
        self.ids.saving.text = ''

class SpendingScreen(SavrScreen):

    def to_spending_screen(self, category):
        main_screen_manager.current = 'input_spending'
        main_screen_manager.get_screen('input_spending').category_name = "Total {} spent:".format(category)
        main_screen_manager.get_screen('input_spending').category = category


class InputScreen(SavrScreen):

    category_name = StringProperty("Total {} spent:")
    category = StringProperty("")

    def confirm_amount(self,amount):
        self.local_usr.spending_total += float(amount)
        self.local_usr.spending[self.category] += float(amount)
        self.ids.amount.text = ''

class ProfileScreen(SavrScreen):


    def user_name(self):
        main_screen_manager.current = 'profile'
        main_screen_manager.get_screen('profile').user_name = "Name: {} \nAge: {} \nLeaderboard Rank: {}".format(local_user.name, local_user.age, local_user.rank)

    goal_description = StringProperty("Goals")

    user_name = StringProperty("Name: {} \nAge: {} \nLeaderboard Rank: {}".format(local_user.name, local_user.age, local_user.rank))

    def savr_points(self):
        main_screen_manager.current = 'profile'
        main_screen_manager.get_screen('profile').savr_points = "Savrpoints: {}".format(local_user.savrpoints)

    savr_points = StringProperty("Savrpoints: {}".format(local_user.savrpoints))

    def Goals(self):
        main_screen_manager.current = 'profile'
        main_screen_manager.get_screen('profile').goals = "{}\n By {} you've got to save ${}! This is something we know you can do!".format(local_user.goals)

    Goals = StringProperty("Goals: {}".format(local_user.goals))

    def my_spending(self):
        main_screen_manager.current = 'see_spending'
        main_screen_manager.get_screen('see_spending').my_spending = "{}"

    def food(self):
        main_screen_manager.current = 'see_spending'
        main_screen_manager.get_screen('see_spending').food = "{}"

    def transport(self):
        main_screen_manager.current = 'see_spending'
        main_screen_manager.get_screen('see_spending').transport= "{}"

    def entertainment(self):
        main_screen_manager.current = 'see_spending'
        main_screen_manager.get_screen('see_spending').entertainment = "{}"

    def personal_care(self):
        main_screen_manager.current = 'see_spending'
        main_screen_manager.get_screen('see_spending').personal_care = "{}"

    def other(self):
        main_screen_manager.current = 'see_spending'
        main_screen_manager.get_screen('see_spending').other = "{}"

    def saving(self):
        main_screen_manager.current = 'profile'
        main_screen_manager.get_screen('profile').saving = "{}".format(local_user.saving)

    def on_pre_enter(self, *args):
        self.refresh(self.local_usr)

    def refresh(self,local_usr):
        self.local_usr = local_usr
        self.user_name = "Name: {} \nAge: {} \nLeaderboard Rank: {}".format(local_usr.name, local_usr.age, local_usr.rank)
        self.savr_points = "Savrpoints: {}".format(local_usr.savrpoints)
        self.goals = "Goals: {}".format(local_usr.goals)
        self.saving = "Savings: {}".format(self.local_usr.saving)
        if len(self.local_usr.goals) > 0:
            self.goal_description = "Goals:\nBy {} \nyou've got to save ${}! \nThis is something \nwe know you can do!".format(local_usr.goals[0]["date_to_complete_by"],local_usr.goals[0]["goal_amount"])
        else:
            self.goal_description = "Goals"

    saving = StringProperty("Savings: {}".format(local_user.saving))

class SeeSpendingScreen(SavrScreen):

    my_spending = StringProperty("{}".format(local_user.spending_total))
    food = StringProperty("{}".format(local_user.spending['Food/Drinks']))
    transport = StringProperty("{}".format(local_user.spending['Transport']))
    entertainment = StringProperty("{}".format(local_user.spending['Entertainment']))
    personal_care = StringProperty("{}".format(local_user.spending['Personal Care']))
    other = StringProperty("{}".format(local_user.spending['Other']))

    def on_pre_enter(self, *args):
        self.refresh(self.local_usr)

    def refresh(self,local_usr):
        self.my_spending = "{}".format(local_usr.spending_total)
        self.food = "{}".format(local_usr.spending["Food/Drinks"])
        self.transport = "{}".format(local_usr.spending["Transport"])
        self.entertainment = "{}".format(local_usr.spending["Entertainment"])
        self.personal_care = "{}".format(local_usr.spending["Personal Care"])
        self.other = "{}".format(local_usr.spending["Other"])
        self.local_usr = local_usr




class QuizScreen(SavrScreen):

    class Question:

        def __init__(self,question,answer,level=1):
            self.question = question
            self.answer = answer
            self.level = level

            self.answer_box = None

            self.questions = []

        def check(self):
            return self.answer.lower() in self.answer_box.text.lower() or self.answer_box.text.lower() in self.answer.lower()

    def on_pre_enter(self, *args):
        self.quiz_container = self.ids.quiz_container
        self.to_delete = []
        self.generate_questions()

    def check_quiz(self, a):
        for q in self.questions:
            if q.check():
                self.local_usr.savrpoints += q.level*20

        main_screen_manager.current = 'profile'

        # Delete questions
        for i in self.to_delete:
            self.quiz_container.remove_widget(i)


    def generate_questions(self):
        self.questions = [self.Question("What is a bank","the","put withdraw take money"),
                     self.Question("What is an easy way to prevent impulse buying?","cash not card"),
                     self.Question("How do you gets lots of money?","save"),
                     self.Question("What is meant by saving up for a rainy day?", "lots large")]

        count = 0
        for question in self.questions:
            count += 1
            q = Label(text=question.question, color=(0,0,0,1))
            self.quiz_container.add_widget(q)
            a = TextInput(id="Q{}".format(count))
            question.answer_box = a
            self.quiz_container.add_widget(a)

            self.to_delete.append(a)
            self.to_delete.append(q)

        button = Button(text="Check Answers",on_press=self.check_quiz)
        self.quiz_container.add_widget(button)

        self.to_delete.append(button)



class GoalScreen(SavrScreen):

    def on_pre_enter(self, *args):
        self.refresh(self.local_usr)
        self.to_delete = []

        for goal in self.local_usr.goals:
            g = Label(text="{}\n${}\nComplete by {}".format(goal['goal_description'], goal['goal_amount'], goal['date_to_complete_by']), color=(0, 0, 0, 1),
                        size_hint=(1, 0.1), font_size = 16)
            self.ids.goals.add_widget(g)
            self.to_delete.append(g)

    def on_leave(self, *args):
        for widget in self.to_delete:
            self.remove_widget(widget)

    def confirm_data(self, goal_description, goal_amount, date_to_complete_by):

        new_goal = {'goal_description': goal_description,'goal_amount':goal_amount,'date_to_complete_by':date_to_complete_by}
        self.local_usr.goals.append(new_goal)

        self.ids.goal_description.text = ''
        self.ids.goal_amount.text = ''
        self.ids.date_to_complete_by.text = ''

    def goal_description(self):
        main_screen_manager.current = 'goal'
        main_screen_manager.get_screen('goal').goal_description = "{}\n By {} you've got to save ${}! This is something we know you can do!".format(local_user.goals[goal_description])


# Add the screens, with their relevant title
for _screen, _name in zip([WelcomeScreen,LoginScreen,SignUpScreen,HomeScreen,BudsScreen,SavingsScreen,SpendingScreen,InputScreen,ProfileScreen,SeeSpendingScreen, QuizScreen, GoalScreen],
                          ['welcome','login','signup','home','buds','savings','spending', 'input_spending','profile','see_spending', 'quiz', 'goal']):
    main_screen_manager.add_widget(_screen(name=_name))









# Main App class :

class SavrBudsApp(App):

    def build(self):
        return main_screen_manager

if __name__ == "__main__":
    SavrBudsApp().run()