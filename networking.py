from kivy.network.urlrequest import UrlRequest
import urllib
import json


is_local = True

# Dummy server
class LocalDatabase:

    def __init__(self):
        self.users = [{'name':'Allie','password':'I<3Lew','id':101,'email':'a@g.com','age':19,'savrpoints':200,
                       'spending_total':300, 'spending':{'Food/Drinks':90,'Transport':200,'Entertainment':10,
                                                'Personal Care':0,'Other':0},'tree':80,'saving':8763.00,
                       'goals':[{'goal_description': 'To go to Japan','goal_amount': 10800 ,'date_to_complete_by': '20th September 2015'}],'rank':1,'acheivement':[''],"friends":[{"name":"Lewis","savrpoints":100},
                                                                           {"name":"Stuart","savrpoints":670}]},

                      {'name':'Lewis','password':'I<3Ali','id':102,'email':'l@g.com','age':18,'savrpoints':100,
                       'spending_total':200, 'spending':{'Food/Drinks':80,'Transport':40,'Entertainment':0,
                                            'Personal Care':80,'Other':0},'tree':80,
                       'saving':20.00,'goals':[{'goal_description': 'To get burritos','goal_amount': 10,'date_to_complete_by': '7th August 2018'}],'rank':2,'acheivement':[''],'friends':[{"name":"Allie","savrpoints":200},
                                                                           {"name":"Stuart","savrpoints":670}]},

                      {'name': 'Stuart', 'password': 'I<3Ali', 'id': 103, 'email': 'l@g.com', 'age': 18,
                       'savrpoints': 100, 'spending_total': 200,
                       'spending': {'Food/Drinks': 80, 'Transport': 40, 'Entertainment': 0, 'Personal Care': 80,
                                    'Other': 0}, 'tree': 80, 'saving': 20.00, 'goals': [], 'rank': 2,
                       'acheivement': [{'goal_description': 'To pay for a Europe Trip' ,'goal_amount': 5000,'date_to_complete_by': '24th November 2018'}], 'friends': [103]}
        ]



    def new_id(self):
        potential_id = 100
        count = 0
        while count < len(self.users):
            if self.users[count]['id'] == potential_id:
                count = 0
                potential_id += 1
            count += 1
        return potential_id

    def get(self,parameters):
        # Duplicate of server code
        if parameters['subject'] == 'login':

            exist = None
            for entry in self.users:
                if parameters['name'] == entry['name']:
                    exist = entry
            if exist is None:
                return "User does not exist"

            if exist['password'] == parameters['password']:
                return ("Success",exist)
            else:
                return "Incorrect password!"

        elif parameters['subject'] == 'signup':
            id = self.new_id()
            new_dict = {'name':parameters['name'],'password':parameters['password'],'id':id,'email':parameters['email'],'age':parameters['age'],'savrpoints':0, 'spending':{'Food/Drinks':0,'Transport':0,'Entertainment':0,'Personal Care':0,'Other':0}, 'spending_total':0,'tree':0,'saving':0.00,'goals':[],'rank':0,'acheivement':[]}
            self.users.append(new_dict)
            return ("Success",new_dict)

if is_local:
    local_database = LocalDatabase()




class Network:



    def __init__(self,screen,usr):
        self.screen_manage = screen
        self.local_user = usr

    def get(self,parameters,func):

        json_response = None

        if not is_local:
            params = urllib.urlencode(parameters)
            headers = {'Content-type': 'application/x-www-form-urlencoded','Accept': 'application/json'}
            UrlRequest("client-space.000webhostapp.com",on_success=func,req_headers=headers,req_body=params)
        else: # local
            func(None,local_database.get(parameters))


    # Helpers

    def connect_user(self, name,password):
        self.get({"subject":"login","name":name,"password":password} , self.fin_connect_user)

    def fin_connect_user(self,req,result):
        if result[0] == "Success":
            self.screen_manage.current = 'home'
            self.local_user.re_initiate(result[1])
            self.screen_manage.refresh_user_details(self.local_user)



    def create_user(self, name,password,email,age):
        self.get({"subject":"signup","name":name,"password":password,"email":email,"age":age} , self.fin_create_user)

    def fin_create_user(self,req,result):
        if result[0] == "Success":
            self.screen_manage.current = 'home'
            self.local_user.re_initiate(result[1])
            self.screen_manage.refresh_user_details(self.local_user)


